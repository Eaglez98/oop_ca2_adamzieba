import java.util.ArrayList;

public class Mammal extends Pet
{
    private boolean neuteredState;

    /**
     * Constructor(super)
     */
    public Mammal(String type, String name, String breed, int age, String color, String gender,
                  String registryDate, String petID, boolean neuteredState)
    {
        super(type, name, breed, age, color, gender, registryDate, petID);
        this.neuteredState = neuteredState;
    }

    /**
     * getter
     */
    public boolean isNeuteredState() { return neuteredState; }

    /**
     * setter
     */
    public void setNeuteredState(boolean neuteredState)
    {
        this.neuteredState = neuteredState;
    }
}
