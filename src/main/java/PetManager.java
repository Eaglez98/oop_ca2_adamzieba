import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/***********************************************
 * PetManager class contains all methods
 * associated with (Pet's) data.
 * - Pet REGISTER,
 * - Pet ADD,
 * - Pet DISPLAY,
 * - Pet DELETE,
 * - Pet EDIT.
 **********************************************/

public class PetManager //implements InterfacePet
{
    private final List<Pet> petList;
    public PetManager() { this.petList = new ArrayList<>(); }
    Pet newPet = null;

    public  registerPet()
    {
        //Pet newPet = null;

        Scanner sc = new Scanner(System.in);
        int type = validateInt("What's the type of your pet? \n 1)Mammal \n 2)Fish \n 3)Bird ");
        System.out.println("What's the name of your pet? ");
        String name = sc.nextLine();
        System.out.println("What's the breed of your pet? ");
        String breed = sc.nextLine();
        int age = validateInt("What's the age of your pet? ");
        System.out.println("What's the color of your pet? ");
        String color = sc.nextLine();
        System.out.println("What is it's gender? ");
        String gender = sc.nextLine();
        System.out.println("Please provide the date of registration: ");
        String registryDate = sc.nextLine();
        System.out.println("Pet ID is going to be assigned automatically.");
        //String petID = sc.nextLine();

        if(type == 1)
        {
            newPet.setType("Mammal");
            boolean neutered = validateBoolean("Is your Mammal Pet neutered?");
            newPet = new Pet();

        }
        else if(type == 2)
        {
            newPet.setType("Fish");
            int waterInput = validateInt("What is the suitable water type for your fish? " +
                    "\n 1) Salt-Water \n 2) Filtered-water \n 3) Sweet-water");
            WaterType waterType = WaterType.values()[waterInput];
            if(waterInput == 1)
            {
               // WaterType.SaltedWater = newPet
                System.out.println("Salt-water chosen");
            }
            else if(waterInput == 2)
            {
               // WaterType.FilteredWater = newPet
                System.out.println("Filtered-water chosen");
            }
            else if(waterInput == 3)
            {
               // WaterType.SweetWater =newPet
                System.out.println("Sweet-water chosen");
            }
            newPet = new Pet();
        }
        else if(type == 3)
        {
            newPet.setType("Bird");
            int wingspan = validateInt("What's the wingspan length of your Bird?");
            boolean flyAbility = validateBoolean("Is your bird able to fly?");
            newPet = new Pet();
        }

        /**
         * Returns an instance of 'newPet' which now needs to
         * be added to a ArrayList. (Function below)
         */

        petList.add(0, newPet);
        return petList;
    }


    public <newPet>  Pet addPet(newPet)
    {

    }

    public Pet displayPet()
    {
        System.out.println("Not implemented yet");
        return null;
    }

    public Pet deletePet()
    {
        System.out.println("Not implemented yet");
        return null;
    }

    public Pet editPet()
    {
        System.out.println("Not implemented yet");
        return null;
    }

    /***************************************
     * Input-Validation Methods
     ***************************************/
    public int validateInt(String message)
    {
        Scanner sc = new Scanner(System.in);
        while(true)
        {
            try {
                System.out.println(message);
                return sc.nextInt();
            }
            catch(Exception e)
            {
                System.out.println("Invalid input or number out of range." +
                        " please try again " + sc.nextLine() + "'");
            }
        }
    }

    public boolean validateBoolean(String message)
    {
        Scanner sc = new Scanner(System.in);
        while(true)
        {
            try{
                System.out.println(message);
                return sc.nextBoolean();
            }
            catch(Exception e)
            {
                System.out.println("Invalid input, please try again " +
                        sc.nextLine() + "'");
            }
        }
    }

}
