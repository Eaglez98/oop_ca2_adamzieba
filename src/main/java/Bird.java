import java.util.ArrayList;

public class Bird extends Pet
{
    private int wingspan;
    private boolean flyAbility;

    public Bird(String type, String name, String breed, int age, String color, String gender,
                String registryDate, String petID, int wingspan, boolean flyAbility)
    {
        super(type, name, breed, age, color, gender, registryDate, petID);
        this.wingspan = wingspan;
        this.flyAbility = flyAbility;
    }

    /**
     * Getters
     */
    public int getWingspan() { return wingspan; }
    public boolean isFlyAbility() { return flyAbility; }

    /**
     * Setters
     */
    public void setWingspan(int wingspan) { this.wingspan = wingspan; }
    public void setFlyAbility(boolean flyAbility) { this.flyAbility = flyAbility; }
}
