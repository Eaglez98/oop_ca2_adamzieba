import java.util.ArrayList;
import java.util.Objects;

/***************************************************************
 * This is the Pet constructor class.
 ****************************************************************/
public class Pet
{
    private String type;
    private String name;
    private String breed;
    private int age;
    private String color;
    private String gender;
    private String registryDate;
    private String petID;

    private static int count = 0;

    /**
     * Constructor
     */
    public Pet(String type, String name, String breed, int age, String color, String gender,
               String registryDate, String petID)
    {
        this.type = type;
        this.name = name;
        this.breed = breed;
        this.age = age;
        this.color = color;
        this.gender = gender;
        this.registryDate = registryDate;
        this.petID = petID;
    }

    public Pet()
    {
        this.type = type;
        this.name = " ";
        this.breed = " ";
        this.age = age;
        this.color = color;
        this.gender = " ";
        this.registryDate = " ";

        /**
         * Automatically assigns +1 to the Pet's ID
         * along with a new instance of Pet.
         */
        this.petID = "PET" + count;
        count++;
    }

    /**
     * Getters
     */
    public String getType() { return type; }
    public String getName() { return name; }
    public String getBreed() { return breed; }
    public int getAge() { return age; }
    public String getColour() { return color; }
    public String getGender() { return gender; }
    public String getRegistryDate() { return registryDate; }
    public String getPetID() { return petID; }


    /**
     * Setters
     */
    public void setType(String type) { this.type = type; }
    public void setName(String name) { this.name = name; }
    public void setBreed(String breed) { this.breed = breed; }
    public void setAge(int age) { this.age = age; }
    public void setColor(String color) { this.color = color; }
    public void setGender(String gender) { this.gender = gender; }
    public void setRegistryDate(String registryDate) { this.registryDate = registryDate; }
    public void setPetID(String petID) { this.petID = petID; }

    @Override
    public String toString()
    {
        return "Pet{" +
                "type='" + type + '\'' +
                ", name='" + name + '\'' +
                ", breed='" + breed + '\'' +
                ", age=" + age +
                ", color='" + color + '\'' +
                ", gender='" + gender + '\'' +
                ", registryDate='" + registryDate + '\'' +
                ", petID='" + petID + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pet pet = (Pet) o;
        return age == pet.age &&
                Objects.equals(type, pet.type) &&
                Objects.equals(name, pet.name) &&
                Objects.equals(breed, pet.breed) &&
                Objects.equals(color, pet.color) &&
                Objects.equals(gender, pet.gender) &&
                Objects.equals(registryDate, pet.registryDate) &&
                Objects.equals(petID, pet.petID);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(type, name, breed, age, color, gender, registryDate, petID);
    }
}
