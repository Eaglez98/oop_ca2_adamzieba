import java.util.ArrayList;
import java.util.List;

public class Fish extends Pet
{
    private WaterType waterType;

    public Fish(String type, String name, String breed, int age, String color, String gender, String registryDate,
                String petID, WaterType waterType)
    {
        super(type, name, breed, age, color, gender, registryDate, petID);
        this.waterType = waterType;
    }

    public Fish(WaterType waterType) { this.waterType = waterType; }

    /**
     * Getter
     */
    public WaterType getWaterType() { return waterType; }

    /**
     * Setter
     */
    public void setWaterType(WaterType waterType) { this.waterType = waterType; }
}


