/***********************************************************
 * Enum class for water types used by the Fish child class.
 ***********************************************************/
public enum WaterType
{
    SaltedWater,
    FilteredWater,
    SweetWater;
}
