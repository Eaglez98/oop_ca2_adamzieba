/*******************************************************************
 * Pet interface with abstract necessary methods for Pet Manager.
 ******************************************************************/
public interface InterfacePet
{
    Pet registerPet();
    Pet addPet();
    Pet displayPet();
    Pet deletePet();
    Pet editPet();
}
