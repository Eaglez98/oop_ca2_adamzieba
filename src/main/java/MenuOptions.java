/*********************************************************
 * Enum class for menu options used in the main file.
 * Main purpose of this enumerator class is to keep the
 * case options static and fixed. Also for code clarity.
 *********************************************************/
public enum MenuOptions
{
    Quit,
    Informations,
    Display,
    ADDownerPet,
    DELETEownerPet,
    EDITownerPet,
    Statistics;
}
