import java.util.List;

/**********************************************
 * This is the Owner constructor class.
 *********************************************/

public class Owner
{
    private String name;
    private String ownerID;
    private String email;
    private int telephone;
    private String homeAddress;
    //protected List<Pet> listOfPets

    /**
     * Constructor
     */
    public Owner(String name, String ownerID, String email, int telephone, String homeAdress)
    {
        this.name = name;
        this.ownerID = ownerID;
        this.email = email;
        this.telephone = telephone;
        this.homeAddress = homeAdress;
    }

    /**
     * Getters
     */
    public String getName() { return name; }
    public String getOwnerID() { return ownerID; }
    public String getEmail() { return email; }
    public int getTelephone() { return telephone; }
    public String getHomeAddress() { return homeAddress; }

    /**
     * Setters
     */
    public void setName(String name)
    {
        //if(Owner.)
        this.name = name;
    }

    public void setOwnerID(String ownerID) {
        this.ownerID = ownerID;
    }
    public void setEmail(String email) { this.email = "Email not provided"; }
    public void setTelephone(int telephone) { this.telephone = telephone; }
    public void setHomeAdress(String homeAdress) { this.homeAddress = "Adress not provided"; }


}
