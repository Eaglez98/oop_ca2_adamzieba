import java.util.Scanner;

/****************************************************************
 * This is the Main file which displays User-menu and runs
 * the entire project.
 * Created by Adam Zieba (GD2a)
 ****************************************************************/

public class Main
{
    //Establishes scanner
    public static Scanner sc = new Scanner(System.in);

    public static void main(String[] args)
    {
        boolean quitMenu = false;
        while(quitMenu ==  false)
        {
            printMenu();
            int input = sc.nextInt();
            //This sets a 'default' preventing 'Array out of bounds exception'
            if(input < 0 || input > MenuOptions.values().length - 1)
            {
                input = 0;
                askAgain();
            }
            MenuOptions options = MenuOptions.values()[input];

            switch(options)
            {
                case Quit:
                    quitMenu = true;
                    break;

                case Informations:
                    informations();
                    break;

                case Display:
                    display();
                    break;

    //            case ADDownerPet:
    //                addOwnerOrPet();
    //                break;
    //
    //            case DELETEownerPet:
    //                deleteOwnerOrPet();
    //                break;
    //
    //            case EDITownerPet:
    //                editOwnerOrPet();
    //                break;
    //
    //            case Statistics:
    //                generateStatistics();
    //                break;
    //
                default:
            }
        }
    }

    /********************************************************
     * Methods
     ********************************************************/
    public static void printMenu()
    {
        System.out.println("////////////////////////////////////////////////");
        System.out.println("///  Please choose one of the below options  ///");
        System.out.println("////////////////////////////////////////////////");
        System.out.println("///  Option 1: Print Application INFO        ///");
        System.out.println("///  Option 2: Display Owners / Pets         ///");
        System.out.println("///  Option 3: ADD Owner / Pets              ///");
        System.out.println("///  Option 4: DELETE Owner / Pets           ///");
        System.out.println("///  Option 5: EDIT Owner / Pets             ///");
        System.out.println("///  Option 6: Print Pet Statistics          ///");
        System.out.println("///  Option 0: Quit The Application          ///");
        System.out.println("////////////////////////////////////////////////");
    }

    public static void informations()
    {
        System.out.println("Informations about the program...");
    }

    public static void askAgain()
    {
        System.out.println("Please choose one of the options" +
                "in the following range: 0 - 6");
    }

    public static void display()
    {

    }



//    public static void addOwnerOrPet() { }
//
//    public static void deleteOwnerOrPet() { }
//
//    public static void editOwnerOrPet() { }
//
//    public static void generateStatistics() { }
//
//    public static void quit()
//    {
//        boolean quitMenu = false;
//        System.out.println("Are you sure you want to quit?");
//        if(quitMenu == false)
//        {
//            printMenu();
//        }
//        else
//        {
//            System.out.println("Goodbye :)");
//            quitMenu = true;
//
//        }
//    }

    /********************************************
     *  User - Input Validation methods
     *********************************************/
    public static void stringValidation()
    {
        Scanner scanner = new Scanner(System.in);
    }
    public static void numberValidation()
    {
        Scanner scanner = new Scanner(System.in);
    }

}
