/****************************************************************************/
                                    README
/****************************************************************************/


This is a text file I created for extra, outside project scope information such
as acknowledgements and thoughts on what I could've done better If my knowledge
of coding was a bit better.



                            Link to Online Repository
/****************************************************************************/
        https://bitbucket.org/Eaglez98/oop_ca2_adamzieba/src/master/



                               Acknowledgements
/****************************************************************************/
I hugely wanted to thank my OOP lecturer ( John Loane ) for delivering a amazing
high quality in-class coding practises and knowledge.

I also wanted to thank ( Konstantins Loginovs ) an ITLC tutorial lecturer for
helping me out and explaining different concepts of java which increased my
understanding and coding confidence.



                               Code - Resources
/****************************************************************************/

(Main) ------- MenuOptions options = MenuOptions.values()[input]; ---------

(Konstantins Loginovs) has helped me to fix my MainOptions enumeration problem
by providing me this line of code I have used in my project.